<?php

namespace yiicod\auth\components;

use CUserIdentity;
use CDbCriteria;
use Yii;

/**
 * UserIdentity class
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 */
class UserIdentity extends CUserIdentity
{

    /**
     * Return user id
     *
     * @author Valeriy Zavolodko <vals2004@gmail.com> 
     *
     * @return integer
     */
    public function getId()
    {
        return $this->getState('id');
    }

    /**
     * Returns the display name for the identity.
     * The default implementation simply returns {@link username}.
     * This method is required by {@link IUserIdentity}.
     * @return string the display name for the identity.
     */
    public function getName()
    {
        return $this->getState('name');
    }

    /**
     * Authenticates a user.
     *
     * @author Valeriy Zavolodko <vals2004@gmail.com>
     * @author Orlov Alexey <aaorlov88@gmail.com>
     * 
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $modelUser = Yii::app()->getComponent('auth')->modelMap['User']['class'];
        $encrypt = Yii::app()->getComponent('auth')->methodEncrypt;
        $criteria = new CDbCriteria();
        $criteria->condition = Yii::app()->getComponent('auth')->modelMap['User']['fieldLogin'] . '=:login AND ' . Yii::app()->getComponent('auth')->modelMap['User']['fieldPassword'] . '=:password';
        $criteria->params = [':login' => $this->username, ':password' => $modelUser::model()->$encrypt($this->password)];

        if (is_array(Yii::app()->getComponent('auth')->criteria)) {
            $criteria->mergeWith(new CDbCriteria(Yii::app()->getComponent('auth')->criteria));
        }
        $user = $modelUser::model()->find($criteria);

        if (null !== $user) {
            $this->setState('id', $user->id);
            $this->setState('name', $this->evaluateExpression(Yii::app()->getComponent('auth')->modelMap['User']['nicknameExpression'], ['model' => $user])); // $user->getLogin());
            //$this->setState('model', $user);
            $this->errorCode = self::ERROR_NONE;
            return true;
        } else {
            $this->errorMessage = Yii::t('auth', 'Incorrect username or password');
            return false;
        }
    }

}
