<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::t('auth', '{name} - Login', ['{name}' => Yii::app()->name]);
$this->breadcrumbs = [
    Yii::t('auth', 'Login'),
];
?>
<div class="content-box">
    <div class="frame">
        <div class="general">
            <div class="general-content">
                <h1 class="yiicod\auth-ttl"><?php echo Yii::t('auth', 'Welcome') ?></h1>
                <?php $this->renderPartial('yiicod.auth.views.webUser._notification') ?>
                <div class="form popup-form">
                    <?php
                    $form = $this->beginWidget('CActiveForm', [
                        'id' => 'login-form',
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
//                            'validateOnChange' => false,
                        ],
                    ]);
                    ?>

                    <div class="form-body">
                        <div class="f-row">
                            <label for="application_modules_auth_models_LoginForm_username">Email</label>
                            <div class="f-input">
                                <?php echo $form->textField($model, 'username'); ?>
                                <div class="errors-box">
                                    <?php echo $form->error($model, 'username'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="f-row">
                            <label for="application_modules_auth_models_LoginForm_password">Password</label>
                            <div class="f-input">
                                <?php echo $form->passwordField($model, 'password'); ?>
                                <div class="errors-box">
                                    <?php echo $form->error($model, 'password'); ?>
                                </div>
                            </div>
                        </div>


                        <div class="f-row rememberMe">
                            <?php echo $form->checkBox($model, 'rememberMe'); ?>
                            <?php echo $form->labelEx($model, 'rememberMe'); ?>
                            <?php echo $form->error($model, 'rememberMe'); ?>
                        </div>
                        <div class="f-buttons clearfix">
                            <?php echo CHtml::link('Forgot password', $this->createAbsoluteUrl('forgot')); ?>
                            <?php echo CHtml::submitButton('Login'); ?>
                        </div>
                        <div class="f-buttons">
                            <?php echo Yii::t('auth', 'Don\'t have an account') ?> <?php echo CHtml::link(Yii::t('auth', 'Sign Up'), $this->createAbsoluteUrl('signup')); ?>
                        </div>

                    </div>


                    <?php $this->endWidget(); ?>
                </div><!-- form -->

            </div>

        </div>

    </div>
</div>



