<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::t('auth', '{name} - Signup', ['{name}' => Yii::app()->name]);
?>
<div class="content-box">
    <div class="frame">
        <div class="general">
            <div class="general-content">                
                <h1 class="yiicod\auth-ttl"><?php echo Yii::t('auth', 'Sign Up') ?></h1>
                <?php $this->renderPartial('yiicod.auth.views.webUser._notification') ?>
                <?php
                $form = $this->beginWidget('CActiveForm', [
                    'id' => 'register-form',
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'validateOnSubmit' => true,
//                        'validateOnChange' => false,
                    ],
                    'htmlOptions' => [
                        'autocomplete' => 'off',
                        'class' => 'popup-form'
                    ]
                ]);
                ?>

                <div class="form-body">
                    <div class="f-row ">
                        <?php echo $form->labelEx($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail']) ?>
                        <div class="f-input">
                            <?php echo $form->textField($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail']); ?>
                            <div class="errors-box">
                                <?php echo $form->error($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail']); ?>
                            </div>
                        </div>

                    </div>

                    <div class="f-row ">
                        <?php echo $form->labelEx($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldPassword']) ?>
                        <div class="f-input">
                            <?php echo $form->passwordField($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldPassword']); ?>
                            <div class="errors-box">
                                <?php echo $form->error($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldPassword']); ?>
                            </div>
                        </div>

                    </div>

                    <div class="f-row ">
                        <?php echo $form->labelEx($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldConfirmPassword']) ?>
                        <div class="f-input">
                            <?php echo $form->passwordField($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldConfirmPassword']); ?>
                            <div class="errors-box">
                                <?php echo $form->error($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldConfirmPassword']); ?>
                            </div>
                        </div>

                    </div>
                    <?php if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldFirstName'], $model->attributeNames())) : ?>
                        <div class="f-row ">
                            <?php echo $form->labelEx($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldFirstName']) ?>
                            <div class="f-input">
                                <?php echo $form->textField($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldFirstName']); ?>
                                <div class="errors-box">
                                    <?php echo $form->error($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldFirstName']); ?>
                                </div>
                            </div>

                        </div>
                    <?php endif; ?>
                    <?php if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldLastName'], $model->attributeNames())) : ?>
                        <div class="f-row ">
                            <?php echo $form->labelEx($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldLastName']) ?>
                            <div class="f-input">
                                <?php echo $form->textField($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldLastName']); ?>
                                <div class="errors-box">
                                    <?php echo $form->error($model, Yii::app()->getComponent('auth')->modelMap['User']['fieldLastName']); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="f-buttons clearfix">
                        <?php echo CHtml::submitButton(Yii::t('auth', 'Registration')); ?>
                    </div>

                </div>


                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>


