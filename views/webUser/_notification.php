<?php
if (file_exists(Yii::getPathOfAlias('bootstrap.widgets.TbAlert') . '.php')):
    $this->widget('bootstrap.widgets.TbAlert', [
        'block' => true,
        'fade' => true,
        'closeText' => '&times;', // false equals no close link
        'events' => [],
        'htmlOptions' => ['class' => 'notification'],
        'userComponentId' => 'user',
        'alerts' => [// configurations per alert type
            // success, info, warning, error or danger
            'success' => ['closeText' => '&times;'],
            'info', // you don't need to specify full config
            'warning' => ['closeText' => '&times;'],
            'error' => ['closeText' => '&times;']
        ],
    ]);
    ?>
<?php else: ?>
    <?php if (Yii::app()->user->hasFlash('error')):
        ?>
        <div class="flash-error">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php elseif (Yii::app()->user->hasFlash('success')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
