<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - '. Yii::t('auth', 'Forgot password');
$this->breadcrumbs = [
    Yii::t('auth', 'Forgot password'),
];
?>
<div class="content-box">
    <div class="frame">
        <div class="general">
            <div class="general-content">
                <h1 class="yiicod\auth-ttl"><?php echo Yii::t('auth', 'Forgot password') ?></h1>
                <?php $this->renderPartial('yiicod.auth.views.webUser._notification') ?>
                <div class="form popup-form">
                    <?php
                    $form = $this->beginWidget('CActiveForm', [
                        'id' => 'recovery-form',
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
//                            'validateOnChange' => false,
                        ],
                    ]);
                    ?>

                    <div class="form-body">
                        <div class="f-row">
                            <?php echo $form->labelEx($model, 'email'); ?>
                            <div class="f-input">
                                <?php echo $form->textField($model, 'email'); ?>
                                <div class="errors-box">
                                    <?php echo $form->error($model, 'email'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="f-buttons clearfix">
                            <?php echo CHtml::submitButton(Yii::t('auth', 'Proceed...')); ?>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                </div><!-- form -->
            </div>
        </div>
    </div>
</div>
