Auth extensions
===============

Didn't tested with different url rules

If you want install to extensions folder, insert into composer.json:
--------------------------------------------------------------------
```php
"require": {
    "composer/installers": "1.0.3"
}
```

Config ( This is all config for extensions )
---------------------------------------------

```php
'components' => array(
    'auth' => array(
        'class' => 'yiicod\auth\Auth',
        'modelMap' => array(
            'User' => array(
                'alias' => 'yiicod\auth\models\UserModel',
                'class' => 'yiicod\auth\models\UserModel',
                'fieldLogin' => 'email', //requred
                'fieldEmail' => 'email', //requred
                'fieldPassword' => 'password', //requred
                'fieldConfirmPassword' => 'confirm', //requred
                'fieldFirstName' => 'firsName',
                'fieldLastName' => 'lastName',
                'fieldRecoveryKey' => 'recoveryKey', //requred
                'nicknameExpression' => '$model->getLogin()' //requred
            )
        ),
        'controllers' => array(
            'controllerMap' => array(
                'default' => array(
                    'webUser' => 'yiicod\auth\controllers\WebUserController',
                ),
            ),
            'default' => array(
                'webUser' => array(
                    'layout' => '',
                    'filters' => array(),
                    'accessRules' => array(),
                ),
            )
        ),
        'methodEncrypt' => 'encrypt',
        'userIdentity' => 'yiicod\auth\components\UserIdentity',
        'authUserBehavior' => 'yiicod\auth\controllers\behaviors\AuthUserBehavior',
    )
)

'preload' => array('auth')
```

Using
-----

After setting up the config better, but not necessarily to create a module **yiicod\auth**. 
In the configuration of the extension soon as it is possible to see him. 
You can set in config: "criteria", this is criteria will be work for all
action: login, forgot, etc... Or you can set criteria for only login action or forgot, etc...
