<?php

namespace yiicod\auth\controllers;

use CMap;
use Controller;
use Yii;

/**
 * WebUser controller
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
class WebUserController extends WebUserBase
{

    public $defaultAction = 'login';

    public function init()
    {
        parent::init();

        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        $this->layout = Yii::app()->getComponent('auth')->controllers[$module][Yii::app()->controller->id]['layout'];
    }

    public function filters()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::filters(), Yii::app()->getComponent('auth')->controllers[$module][Yii::app()->controller->id]['filters']
        );
    }

    public function accessRules()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::accessRules(), Yii::app()->getComponent('Comments')->controllers[$module][Yii::app()->controller->id]['accessRules']
        );
    }

    /**
     * Declares class-based actions.
     * For change functional use AuthUserBehavior, This is component
     * declaret in to config.php, you can change him.
     * Auth event:
     * 
     * - beforeLogin(CEvent)
     * - afterLogin(CEvent)
     * - errorLogin(CEvent)
     * 
     * - beforeSignup(CEvent)
     * - afterSignup(CEvent)
     * - errorSignup(CEvent)
     * 
     * - beforeCheckRecoveryKey(CEvent)
     * - afterCheckRecoveryKey(CEvent)
     * - errorCheckRecoveryKey(CEvent)
     * 
     * - beforeForgot(CEvent)
     * - afterForgot(CEvent)
     * - errorForgot(CEvent)
     * 
     * - beforeLogout(CEvent)
     * - afterLogout(CEvent)
     * 
     * Also if you use extensions evenement, use this events,
     * Insert into listeners.php:
     * 
     * ...
     * yiicod.auth.controllers.WebUserBase.[All event name before]
     * ...
     * 
     */
    public function actions()
    {

        return CMap::mergeArray(parent::actions(), [
                    'login' => [
                        'class' => 'yiicod\auth\actions\webUser\LoginAction',
                    ],
                    'forgot' => [
                        'class' => 'yiicod\auth\actions\webUser\ForgotAction',
                    ],
                    'logout' => [
                        'class' => 'yiicod\auth\actions\webUser\LogoutAction',
                    ],
                    'signup' => [
                        'class' => 'yiicod\auth\actions\webUser\SignupAction',
                    ],
                    'checkRecoveryKey' => [
                        'class' => 'yiicod\auth\actions\webUser\CheckRecoveryKeyAction',
                    ],
                        ]
        );
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), [
                    'AuthUserBehavior' => [
                        'class' => Yii::app()->getComponent('auth')->authUserBehavior
                    ]
        ]);
    }

}
