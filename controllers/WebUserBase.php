<?php

namespace yiicod\auth\controllers;

use CMap;
use Controller;
use Yii;

class WebUserBase extends Controller
{

    protected function attachEvent($name, $event)
    {
        if (null === Yii::app()->getComponent('emitter')) {
            return false;
        }
        Yii::app()->emitter->emit($name, [
            $event
        ]);
    }

    /**
     * Action before login
     * @param 
     */
    public function onBeforeLogin($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.beforeLogin', $event);
        $this->raiseEvent('onBeforeLogin',$event);        
    }

    /**
     * Action before signup
     * @param CModel
     */
    public function onBeforeSignup($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.beforeSignup', $event);
        $this->raiseEvent('onBeforeSignup',$event);        
    }

    /**
     * Action before forgot
     * @param CModel
     */
    public function onBeforeForgot($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.beforeForgot', $event);
        $this->raiseEvent('onBeforeForgot',$event);        
    }

    /**
     * Action before check recovery key
     * @param CModel
     */
    public function onBeforeCheckRecoveryKey($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.beforeCheckRecoveryKey', $event);
        $this->raiseEvent('onBeforeCheckRecoveryKey',$event);        
    }

    public function onBeforeLogout($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.beforeLogout', $event);
        $this->raiseEvent('onBeforeLogout',$event);        
    }

    /**
     * Action after forgot
     * @param CModel
     */
    public function onAfterForgot($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.afterForgot', $event);
        $this->raiseEvent('onAfterForgot',$event);        
    }

    /**
     * Action after check recovery key
     * @param CModel
     */
    public function onAfterCheckRecoveryKey($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.afterCheckRecoveryKey', $event);
        $this->raiseEvent('onAfterCheckRecoveryKey',$event);        
    }

    /**
     * Action after login
     * @param CModel
     */
    public function onAfterLogin($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.afterLogin', $event);
        $this->raiseEvent('onAfterLogin',$event);        
    }

    /**
     * Action after signup
     * @param CModel
     */
    public function onAfterSignup($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.afterSignup', $event);
        $this->raiseEvent('onAfterSignup',$event);        
    }

    public function onAfterLogout($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.afterLogout', $event);
        $this->raiseEvent('onAfterLogout',$event);        
    }

    /**
     * Action error check recovery key
     * @param CModel
     */
    public function onErrorCheckRecoveryKey($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.errorCheckRecoveryKey', $event);
        $this->raiseEvent('onErrorCheckRecoveryKey',$event);        
    }

    /**
     * Action error forgot
     * @param CModel
     */
    public function onErrorForgot($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.errorForgot', $event);
        $this->raiseEvent('onErrorForgot',$event);        
    }

    /**
     * Action error check recovery key
     * @param CModel
     */
    public function onErrorLogin($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.errorLogin', $event);
        $this->raiseEvent('onErrorLogin',$event);        
    }

    /**
     * Action error forgot
     * @param CModel
     */
    public function onErrorSignup($event)
    {
        $this->attachEvent('yiicod.auth.controllers.WebUserBase.errorSignup', $event);
        $this->raiseEvent('onErrorSignup',$event);        
    }

}
