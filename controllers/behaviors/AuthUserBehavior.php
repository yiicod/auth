<?php

namespace yiicod\auth\controllers\behaviors;

/**
 * Auth behavior with event for controller action
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
use Yii;
use CHtml;

class AuthUserBehavior extends AuthBaseBehavior
{

    /**
     * After login action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function afterLogin($event)
    {
        parent::afterLogin($event);

        if (Yii::app()->authManager->checkAccess('admin', Yii::app()->user->getId())) {
            $event->sender->getController()->createAbsoluteUrl('/admin');
        } else {
            $event->sender->getController()->createAbsoluteUrl(Yii::app()->getHomeUrl());
        }
    }

    /**
     * After signup action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function afterSignup($event)
    {
        parent::afterSignup($event);

        $password = $_POST[CHtml::modelName($event->params['model'])]['password'];
        $userIdentity = Yii::app()->getComponent('auth')->userIdentity;
        $identity = new $userIdentity($event->params['model']->email, $password);
        if ($identity->authenticate()) {
            Yii::app()->user->login($identity);
            $event->sender->getController()->redirect(Yii::app()->getHomeUrl());
        }
    }

    /**
     * After forgot action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function afterForgot($event)
    {
        parent::afterForgot($event);

        $resetUrl = $event->sender->getController()->createAbsoluteUrl('checkRecoveryKey', ['recoveryKey' => $event->params['model']->getRecoveryKey()]);
        $subject = Yii::t("auth", "You have requested the password recovery site {siteName}", [
                    '{siteName}' => Yii::app()->name,
        ]);
        $message = Yii::t("auth", "You have requested the password recovery site {siteName}. To receive a new password, go to {resetUrl}", [
                    '{siteName}' => Yii::app()->name,
                    '{resetUrl}' => $resetUrl,
        ]);

        Yii::app()->phpMailer->send($event->params['model']->email, $subject, $message);
        Yii::app()->user->setFlash('success', Yii::t("auth", "Please check your email. An instructions was sent to your email address."));
    }

    /**
     * After checkRecoveryKey action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel, 'password' => 'Not encrypt password')
     */
    public function afterCheckRecoveryKey($event)
    {
        $subject = Yii::t("auth", "Now you have new password in site {siteName}", [
                    '{siteName}' => Yii::app()->name,
        ]);
        $message = Yii::t("auth", "Now you have new password in site {siteName}. Please login with your new password: {newPassword}", [
                    '{siteName}' => Yii::app()->name,
                    '{newPassword}' => $event->params['password']
        ]);

        Yii::app()->phpMailer->send($event->params['model']->email, $subject, $message);
        Yii::app()->user->setFlash('success', Yii::t("auth", "New password sent to your email."));
        $event->sender->getController()->redirect(['login']);
    }

    /**
     * Before login action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function beforeLogin($event)
    {
        parent::beforeLogin($event);
    }

    /**
     * Before signup action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function beforeSignup($event)
    {
        parent::beforeSignup($event);
    }

    /**
     * Before forgot action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function beforeForgot($event)
    {
        parent::beforeForgot($event);
    }

    /**
     * Before checkRecoveryKey action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel, 'password' => 'Not encrypt password')
     */
    public function beforeCheckRecoveryKey($event)
    {
        parent::beforeCheckRecoveryKey($event);
    }

    /**
     * error checkRecoveryKey action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function errorCheckRecoveryKey($event)
    {
        parent::errorCheckRecoveryKey($event);

        Yii::app()->user->setFlash('error', Yii::t("auth", "Incorrect recovery link."));
        $event->sender->getController()->redirect('forgot');
    }

    /**
     * Error forgot action event
     * @param CEvent $event Object has next params sender -> LoginAction, 
     * params -> array('model' => UserModel)
     */
    public function errorForgot($event)
    {
        parent::errorForgot($event);

        Yii::app()->user->setFlash('error', Yii::t("auth", "Not found user"));
        $event->sender->getController()->redirect('forgot');
    }

}
