<?php

namespace yiicod\auth\controllers\behaviors;

/**
 * Base auth behavior, with all declarate events
 * @author Orlov Alexey <aaorlov@gmail.com>
 */
use CBehavior;

class AuthBaseBehavior extends CBehavior
{

    /**
     * Declares events and the corresponding event handler methods.
     * If you override this method, make sure you merge the parent result to the return value.
     * @return array events (array keys) and the corresponding event handler methods (array values).
     * @see CBehavior::events
     */
    public function events()
    {
        return array_merge(parent::events(), [
            'onBeforeLogin' => 'beforeLogin',
            'onBeforeSignup' => 'beforeSignup',
            'onBeforeForgot' => 'beforeForgot',
            'onBeforeCheckRecoveryKey' => 'beforeCheckRecoveryKey',
            'onBeforeLogout' => 'beforeLogout',
            'onAfterForgot' => 'afterForgot',
            'onAfterCheckRecoveryKey' => 'afterCheckRecoveryKey',
            'onAfterLogin' => 'afterLogin',
            'onAfterSignup' => 'afterSignup',
            'onAfterLogout' => 'afterLogout',
            'onErrorCheckRecoveryKey' => 'errorCheckRecoveryKey',
            'onErrorForgot' => 'errorForgot',
            'onErrorLogin' => 'errorLogin',
            'onErrorSignup' => 'errorSignup',
        ]);
    }

    /**
     * Action before login
     * @param 
     */
    public function beforeLogin($event)
    {
        
    }

    /**
     * Action before signup
     * @param CEvent
     */
    public function beforeSignup($event)
    {
        
    }

    /**
     * Action before forgot
     * @param CEvent
     */
    public function beforeForgot($event)
    {
        
    }

    /**
     * Action before check recovery key
     * @param CEvent
     */
    public function beforeCheckRecoveryKey($event)
    {
        
    }

    public function beforeLogout($event)
    {
        
    }

    /**
     * Action after forgot
     * @param CEvent
     */
    public function afterForgot($event)
    {
        
    }

    /**
     * Action after check recovery key
     * @param CEvent
     */
    public function afterCheckRecoveryKey($event)
    {
        
    }

    /**
     * Action after login
     * @param CEvent
     */
    public function afterLogin($event)
    {
        
    }

    /**
     * Action after signup
     * @param CEvent
     */
    public function afterSignup($event)
    {
        
    }

    public function afterLogout($event)
    {
        
    }

    /**
     * Action error check recovery key
     * @param CEvent
     */
    public function errorCheckRecoveryKey($event)
    {
        
    }

    /**
     * Action error forgot
     * @param CEvent
     */
    public function errorForgot($event)
    {
        
    }

    /**
     * Action error check recovery key
     * @param CEvent
     */
    public function errorLogin($event)
    {
        
    }

    /**
     * Action error forgot
     * @param CEvent
     */
    public function errorSignup($event)
    {
        
    }

}
