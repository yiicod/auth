<?php

namespace yiicod\auth\actions\webUser;

use Yii;
use CAction;
use CEvent;

/**
 * Login action 
 * @author Orlov Alexey <aaorlov@gmail.com>
 */
class LogoutAction extends CAction
{

    public $view = '';

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function run()
    {       
        $this->getController()->onBeforeLogout(new CEvent($this));

        Yii::app()->user->logout();

        $this->getController()->onAfterLogout(new CEvent($this));

        Yii::app()->controller->redirect(Yii::app()->getHomeUrl());
    }

}
