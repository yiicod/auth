<?php

namespace yiicod\auth\actions\webUser;

use Yii;
use yiicod\auth\actions\BaseAction;
use yiicod\auth\models\LoginForm;
use CHtml;
use CEvent;

/**
 * Login action 
 * @author Orlov Alexey <aaorlov@gmail.com>
 */
class LoginAction extends BaseAction
{

    public $view = 'yiicod.auth.views.webUser.login';

    public function run()
    {
        parent::run();
        
        $formClass = Yii::app()->auth->modelMap['LoginForm']['class'];
        $model = new $formClass($this->scenario);
        
        $isLoad = false;
        if(isset($_POST[CHtml::modelName($model)])){
            $model->attributes = $_POST[CHtml::modelName($model)];
            $isLoad = true;
        }        
        
        $this->getController()->onBeforeLogin(new CEvent($this, ['model' => $model]));       
        
        // if it is ajax validation request
        $this->performAjaxValidation($model);

        // collect user input data
        if ($isLoad) {            
            // validate user input and redirect to the previous page if valid            
            if ($model->validate() && $model->login()) {
                $this->getController()->onAfterLogin(new CEvent($this, ['model' => $model]));
            }
            $this->getController()->onErrorLogin(new CEvent($this, ['model' => $model]));
        }
        // display the login form
        Yii::app()->controller->render($this->view, ['model' => $model]);
    }

}
