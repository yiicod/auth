<?php

namespace yiicod\auth\actions\webUser;

use Yii;
use yiicod\auth\actions\BaseAction;
use CDbCriteria;
use yiicod\auth\models\RecoveryForm;
use CHtml;
use CEvent;

/**
 * Forgot action 
 * @author Orlov Alexey <aaorlov@gmail.com>
 */
class ForgotAction extends BaseAction
{

    public $view = 'yiicod.auth.views.webUser.forgot';

    /**
     * Send to email recovery link for
     * @author Orlov Alexey <aaorlov@gmail.com>    
     */
    public function run()
    {
        parent::run();
        
        $form = new RecoveryForm($this->scenario);
        // if it is ajax validation request
        $this->performAjaxValidation($form);

        if (isset($_POST[CHtml::modelName($form)])) {

            $form->attributes = $_POST[CHtml::modelName($form)];
            if ($form->validate()) {
                $modelUser = Yii::app()->getComponent('auth')->modelMap['User']['class'];

                $criteria = new CDbCriteria;
                $criteria->condition = Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail'] . '=:email';
                $criteria->params = [':email' => $form->email];
                if ($this->criteria instanceof CDbCriteria) {
                    $criteria->mergeWith($this->criteria);
                }
                
                $model = $modelUser::model()->find($criteria);

                if (null === $model) {
                    $this->getController()->onErrorForgot(new CEvent($this, ['model' => $model]));
                } else {
                    $this->getController()->onBeforeForgot(new CEvent($this, ['model' => $model]));
                    $model->setRecoveryKey(uniqid('reset_', false));
                    $model->save(false);
                }
                $resetUrl = $this->getController()->createAbsoluteUrl(
                        'checkRecoveryKey', ['recoveryKey' => $model->getRecoveryKey()]
                );
                $this->getController()->onAfterForgot(new CEvent($this, ['model' => $model, 'resetUrl' => $resetUrl]));
            }
        }
        $form->email = '';
        Yii::app()->controller->render($this->view, ['model' => $form]);
    }

}
