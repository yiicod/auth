<?php

namespace yiicod\auth\actions\webUser;

use Yii;
use yiicod\auth\actions\BaseAction;
use CHtml;
use CEvent;

/**
 * Signup action 
 * @author Orlov Alexey <aaorlov@gmail.com>
 */
class SignupAction extends BaseAction
{

    public $view = 'yiicod.auth.views.webUser.signup';

    /**
     * Displays the sign up page
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     */
    public function run()
    {
        parent::run();
        
        $modelUser = Yii::app()->getComponent('auth')->modelMap['User']['class'];
        $model = new $modelUser($this->scenario);

        $isLoad = false;
        if(isset($_POST[CHtml::modelName($model)])){
            $model->attributes = $_POST[CHtml::modelName($model)];
            $isLoad = true;
        }           
        
        $this->getController()->onBeforeSignup(new CEvent($this, ['model' => $model]));
        
        // if it is ajax validation request
        $this->performAjaxValidation($model);
        if ($isLoad) {            
            if ($model->save()) {
                $this->getController()->onAfterSignup(new CEvent($this, ['model' => $model]));
            }
            $this->getController()->onErrorSignup(new CEvent($this, ['model' => $model]));
        }
        Yii::app()->controller->render($this->view, ['model' => $model]);
    }

}
