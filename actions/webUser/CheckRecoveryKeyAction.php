<?php

namespace yiicod\auth\actions\webUser;

use Yii;
use CEvent;
use CDbCriteria;
use yiicod\auth\actions\BaseAction;

/**
 * Check recovery key action 
 * @author Orlov Alexey <aaorlov@gmail.com>
 */
class CheckRecoveryKeyAction extends BaseAction
{

    public $view = '';
    public $criteria = null;

    /**
     * Reset user passwork and send to email new password
     * @param $recoveryKey string Recovery key for resetting customer password     
     */
    public function run()
    {
        //Fix for php 5.4
        if (null === Yii::app()->request->getParam('recoveryKey', null)) {
            throw new \CHttpException('Error params', 400);
        }
        
        parent::run();
        
        $modelUser = Yii::app()->getComponent('auth')->modelMap['User']['class'];

        $criteria = new CDbCriteria;
        $criteria->condition = 'recoveryKey=:recoveryKey';
        $criteria->params = [':recoveryKey' => Yii::app()->request->getParam('recoveryKey', null)];
        if ($this->criteria instanceof CDbCriteria) {
            $criteria->mergeWith($this->criteria);
        }

        $model = $modelUser::model()->find($criteria);

        if (null === $model) {
            $this->getController()->onErrorCheckRecoveryKey(new CEvent($this, ['model' => $model]));
        }

        $password = uniqid('member_', true);

        $model->password = $password;
        $model->setRecoveryKey('');

        $this->getController()->onBeforeCheckRecoveryKey(new CEvent($this, ['model' => $model, 'password' => $password]));

        $model->save(false);

        $this->getController()->onAfterCheckRecoveryKey(new CEvent($this, ['model' => $model, 'password' => $password]));
    }

}
