<?php

namespace yiicod\auth\models;

use CFormModel;
use Yii;

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class RecoveryForm extends CFormModel
{

    public $email;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return [
            // username and password are required
            ['email', 'required'],
            ['email', 'email'],
                //array('email', 'ext.validators.EmptyValidator', 'allowEmpty' => false),
                // password needs to be authenticated
                //array('email', 'checkExists'),
        ];
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('auth', "email"),
        ];
    }

}
