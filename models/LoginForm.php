<?php

namespace yiicod\auth\models;

use yiicod\auth\components\UserIdentity;
use CFormModel;
use Yii;

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{

    public $username;
    public $password;
    public $rememberMe;
    protected $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return [
            // username and password are required
            ['username, password', 'required'],
            // rememberMe needs to be a boolean
            ['rememberMe', 'boolean'],
            // password needs to be authenticated
            ['password', 'authenticate'],
        ];
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('auth', 'User Name'),
            'password' => Yii::t('auth', 'Password'),
            'rememberMe' => Yii::t('auth', 'Remember me next time'),
        ];
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userIdentity = Yii::app()->getComponent('auth')->userIdentity;
            $this->_identity = new $userIdentity($this->username, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', Yii::t('auth', 'Incorrect username or password'));
            }
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_identity === null) {
            $userIdentity = Yii::app()->getComponent('auth')->userIdentity;
            $this->_identity = new $userIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === $userIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else {
            return false;
        }
    }

}
