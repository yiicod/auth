<?php

namespace yiicod\auth\models;

use CDbCriteria;
use CActiveRecord;
use CActiveDataProvider;
use Yii;

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $type
 * @property string $password
 * @property string $dateCreate
 */
class UserModel extends CActiveRecord
{

    public $confirm;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'User';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['email', 'required'],
            ['password', 'length', 'min' => 6, 'allowEmpty' => true],
            ['email, password', 'length', 'max' => 100],
            ['email', 'email'],
            ['email', 'unique'],
            ['password', 'compare', 'compareAttribute' => 'confirm', 'allowEmpty' => false],
            ['password', 'required', 'except' => 'update'],
            ['confirm', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, firstName, lastName, email, type, password, dateCreate', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('auth', 'ID'),
            'firstName' => Yii::t('auth', 'First Name'),
            'lastName' => Yii::t('auth', 'Last Name'),
            'email' => Yii::t('auth', 'Email'),
            'type' => Yii::t('auth', 'Type'),
            'password' => Yii::t('auth', 'Password'),
            'dateCreate' => Yii::t('auth', 'Date Create'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('firstName', $this->firstName, true);
        $criteria->compare('lastName', $this->lastName, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('type', $this->type, true);        
        $criteria->compare('dateCreate', $this->dateCreate, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usermodel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        $behaviors['CTimestampBehavior'] = [
            'class' => 'zii.behaviors.CTimestampBehavior',
            'createAttribute' => 'dateCreate',
            'updateAttribute' => null,
        ];

        $behaviors['PasswordBehavior'] = [
            'class' => 'yiicod\auth\models\behaviors\PasswordBehavior',
            'fieldPassword' => Yii::app()->getComponent('auth')->modelMap['User']['fieldPassword'],
        ];

        $behaviors['UserModelBehavior'] = [
            'class' => 'yiicod\auth\models\behaviors\UserModelBehavior',
        ];

        if (file_exists(Yii::getPathOfAlias('application.models.behaviors.XssBehavior') . '.php')) {
            $behaviors['XssBehavior'] = [
                'class' => 'application.models.behaviors.XssBehavior',
            ];
        }

        return \CMap::mergeArray(parent::behaviors(), $behaviors);
    }

}
