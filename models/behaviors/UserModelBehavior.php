<?php
namespace yiicod\auth\models\behaviors;

use CActiveRecordBehavior;
use Yii;

/**
 * Class get and set for model MailQueue
 */
class UserModelBehavior extends CActiveRecordBehavior
{

    /**
     * Set field username
     * @param string $value
     */
    public function setLogin($value)
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldLogin'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldLogin']} = $value;
        }
    }

    /**
     * Set field username
     * @param string $value
     */
    public function setEmail($value)
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail']} = $value;
        }
    }

    /**
     * Set field password
     * @param string $value
     */
    public function setPassword($value)
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldTo'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldTo']} = $value;
        }
    }

    /**
     * Set field fieldRecoveryKey
     * @param string $value
     */
    public function setRecoveryKey($value)
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldRecoveryKey'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldRecoveryKey']} = $value;
        }
    }

    /**
     * Get field username
     * @return strimg
     */
    public function getLogin()
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldLogin'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldLogin']};
        }
        return '';
    }

    /**
     * Get field status
     * @return strimg
     */
    public function getPassword()
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldPassword'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldPassword']};
        }
        return '';
    }

    /**
     * Get field username
     * @return strimg
     */
    public function getEmail()
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldEmail']};
        }
        return '';
    }

    /**
     * Get field username
     * @return strimg
     */
    public function getRecoveryKey()
    {
        if (in_array(Yii::app()->getComponent('auth')->modelMap['User']['fieldRecoveryKey'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('auth')->modelMap['User']['fieldRecoveryKey']};
        }
        return '';
    }

}