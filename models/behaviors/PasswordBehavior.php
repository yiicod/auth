<?php

namespace yiicod\auth\models\behaviors;

use CActiveRecordBehavior;

/**
 * Behavior for password update
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 */
class PasswordBehavior extends CActiveRecordBehavior
{

    /**
     * @var string Field password
     */
    public $fieldPassword = 'password';

    /**
     * @var string Encrypt method
     */
    public $encrypt = 'md5';

    /**
     * @var string Original password
     */
    protected $originalPassword = null;

    /**
     * Encrypt string
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     */
    public function encrypt($str)
    {
        $method = $this->encrypt;
        return $method($str);
    }

    public function isEncrypt($string)
    {
        switch ($this->encrypt) {
            case 'md5':
                return preg_match('/^[a-f0-9]{32}$/', $string);
                break;
        }
        return false;
    }

    public function afterFind($event)
    {
        if (null !== $this->fieldPassword) {
            $this->originalPassword = $this->getOwner()->{$this->fieldPassword};
            $this->getOwner()->{$this->fieldPassword} = null;
        }

        return parent::afterFind($event);
    }

    public function getPasswordHash()
    {
        return $this->originalPassword;
    }

    public function beforeSave($event)
    {
        if (null !== $this->fieldPassword) {
            if (empty($this->getOwner()->{$this->fieldPassword})) {
                $this->getOwner()->{$this->fieldPassword} = $this->originalPassword;
            } elseif (!$this->isEncrypt($this->getOwner()->{$this->fieldPassword})) {
                $this->getOwner()->{$this->fieldPassword} = $this->encrypt($this->getOwner()->{$this->fieldPassword});
            }
        }
    }

}
