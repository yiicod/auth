<?php

namespace yiicod\auth;

use CApplicationComponent;
use CMap;
use Yii;

class Auth extends CApplicationComponent
{

    /**
     * @var ARRAY model settings
     */
    public $modelMap = [];

    /**
     * @var array Controllers settings
     */
    public $controllers = [];

    /**
     * @var string authUserBehavior class 
     */
    public $authUserBehavior = null;

    /**
     * @var string Encrypt method name
     */
    public $methodEncrypt = 'encrypt';

    /**
     * @var string home Url
     * Deprecate
     */
    public $homeUrl = null;

    /**
     * @var type 
     */
    public $userIdentity = null;

    /**
     *
     * @var type 
     */
    public $criteria = null;

    public function init()
    {
        parent::init();

        //Merge main extension config with local extension config
        $config = include(dirname(__FILE__) . '/config/main.php');
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $this->{$key} = CMap::mergeArray($value, $this->{$key});
            } elseif (null === $this->{$key}) {
                $this->{$key} = $value;
            }
        }

        if (!Yii::app() instanceof \CConsoleApplication) {
            //Merge controllers map
            $route = Yii::app()->urlManager->parseUrl(Yii::app()->request);
            $module = substr($route, 0, strpos($route, '/'));
            if (Yii::app()->hasModule($module) && isset($this->controllers['controllerMap'][$module])) {
                Yii::app()->getModule($module)->controllerMap = CMap::mergeArray($this->controllers['controllerMap'][$module], Yii::app()->getModule($module)->controllerMap);
            } elseif (isset($this->controllers['controllerMap']['default'])) {
                Yii::app()->controllerMap = CMap::mergeArray($this->controllers['controllerMap']['default'], Yii::app()->controllerMap);
            }
        }
        
        Yii::setPathOfAlias('yiicod', realpath(dirname(__FILE__) . '/..'));

        Yii::import($this->modelMap['User']['alias']);
        Yii::import($this->modelMap['LoginForm']['alias']);
    }

}
