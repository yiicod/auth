<?php
return [
    'modelMap' => [
        'User' => [
            'alias' => 'yiicod\auth\models\UserModel',
            'class' => 'yiicod\auth\models\UserModel',
            'fieldLogin' => 'email', //requred
            'fieldEmail' => 'email', //requred
            'fieldPassword' => 'password', //requred
            'fieldConfirmPassword' => 'confirm', //requred
            'fieldFirstName' => 'firsName',
            'fieldLastName' => 'lastName',
            'fieldRecoveryKey' => 'recoveryKey', //requred
            'nicknameExpression' => '$model->getLogin()' //requred
        ],
        'LoginForm' => [
            'alias' => 'yiicod\auth\models\LoginForm',
            'class' => 'yiicod\auth\models\LoginForm',
        ],
    ],
    'controllers' => [
        'controllerMap' => [
            'default' => [
                'webUser' => 'yiicod\auth\controllers\WebUserController',
            ],
        ],
        'default' => [
            'webUser' => [
                'layout' => '',
                'filters' => [],
                'accessRules' => [],
            ],
        ]
    ],
    'methodEncrypt' => 'encrypt',
    'criteria' => [],
    'userIdentity' => 'yiicod\auth\components\UserIdentity',
    'authUserBehavior' => 'yiicod\auth\controllers\behaviors\AuthUserBehavior',
];
